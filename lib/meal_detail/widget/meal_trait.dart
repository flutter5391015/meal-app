import 'package:flutter/material.dart';

class MealTrait extends StatelessWidget {
  final List<String> data;
  const MealTrait({super.key, required this.data});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: const EdgeInsets.all(0),
      itemCount: data.length,
      itemBuilder: (ctx, index) => ListTile(
        title: Row(
          children: [
            Icon(
              Icons.fiber_manual_record,
              size: 10,
              color: Theme.of(context).colorScheme.onBackground,
            ),
            const SizedBox(
              width: 10,
            ),
            Expanded(
              child: Text(
                data[index],
                style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                      color: Theme.of(context).colorScheme.onBackground,
                    ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
