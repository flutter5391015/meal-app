import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:meals_app/meal/provider/fav_meal_provider.dart';

class FavIcon extends ConsumerWidget {
  final String mealId;
  const FavIcon({super.key, required this.mealId});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final favMeals = ref.watch(favoriteMealProvider);
    bool isFav = favMeals.where((el) => el.id == mealId).isNotEmpty;

    return AnimatedSwitcher(
      transitionBuilder: (child, animation) {
        return RotationTransition(
          turns: Tween(
            begin: 0.6,
            end: 1.0,
          ).animate(animation),
          child: child,
        );
      },
      duration: const Duration(milliseconds: 500),
      child: Icon(
        isFav ? Icons.favorite : Icons.favorite_outline,
        key: ValueKey(isFav),
      ),
    );
  }
}
