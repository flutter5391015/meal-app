import 'package:flutter/material.dart';
import 'package:meals_app/meal/model/meal.dart';
import 'package:meals_app/meal/provider/fav_meal_provider.dart';
import 'package:meals_app/meal_detail/widget/favorite_icon.dart';
import 'package:meals_app/meal_detail/widget/meal_trait.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class MealDetailScreen extends ConsumerWidget {
  final Meal meal;
  const MealDetailScreen({super.key, required this.meal});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    void onTapFavHandler(Meal meal) {
      ref.read(favoriteMealProvider.notifier).toggleFavMeal(meal);
    }

    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () => onTapFavHandler(meal),
            icon: FavIcon(
              mealId: meal.id,
            ),
          )
        ],
        title: Text(meal.title),
        flexibleSpace: ColorFiltered(
          colorFilter: ColorFilter.mode(
            Colors.black.withOpacity(0.4),
            BlendMode.srcATop,
          ),
          child: Hero(
            tag: meal.id,
            child: Image(
              image: NetworkImage(meal.imageUrl),
              fit: BoxFit.fill,
              width: double.infinity,
            ),
          ),
        ),
      ),
      body: DefaultTabController(
        length: 2,
        child: Column(
          children: [
            const TabBar(
              tabs: [
                Tab(
                  text: 'Ingredients',
                ),
                Tab(
                  text: 'Step',
                ),
              ],
            ),
            Expanded(
              child: TabBarView(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(16),
                    child: MealTrait(data: meal.ingredients),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16),
                    child: MealTrait(data: meal.steps),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
