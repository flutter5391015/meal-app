import 'package:flutter/material.dart';
import 'package:meals_app/meal/model/meal.dart';
import 'package:meals_app/meal/widget/meal_content.dart';
import 'package:meals_app/meal/widget/meal_shimmer.dart';

class MealScreen extends StatelessWidget {
  final String? title;
  final List<Meal> meals;
  const MealScreen({
    super.key,
    this.title,
    required this.meals,
  });

  @override
  Widget build(BuildContext context) {
    if (title == null) {
      return MealShimmer(
        child: MealContent(
          meals: meals,
        ),
      );
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(title!),
      ),
      body: MealShimmer(
        child: MealContent(
          meals: meals,
        ),
      ),
    );
  }
}
