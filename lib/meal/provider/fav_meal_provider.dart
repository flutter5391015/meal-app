import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:meals_app/meal/model/meal.dart';

class FavMealNotifier extends StateNotifier<List<Meal>> {
  // initiate data
  FavMealNotifier() : super([]);

  void toggleFavMeal(Meal meal) {
    final isMealFavorite = state.contains(meal);
    if (isMealFavorite) {
      state = state.where((el) => el.id != meal.id).toList();
    } else {
      state = [...state, meal];
    }
  }
}

final favoriteMealProvider = StateNotifierProvider<FavMealNotifier, List<Meal>>(
    (ref) => FavMealNotifier());
