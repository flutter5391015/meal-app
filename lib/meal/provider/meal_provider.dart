import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:meals_app/meal/data/dummy_meal.dart';

final mealProvider = Provider((ref) {
  return dummyMeals;
});
