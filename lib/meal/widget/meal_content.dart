import 'package:flutter/material.dart';
import 'package:meals_app/meal/model/meal.dart';
import 'package:meals_app/meal/widget/meal_item.dart';
import 'package:meals_app/meal_detail/meal_detail_screen.dart';

class MealContent extends StatelessWidget {
  final List<Meal> meals;
  const MealContent({super.key, required this.meals});

  void onTapMealHandler({required BuildContext context, required Meal meal}) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (ctx) => MealDetailScreen(
          meal: meal,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget content = ListView.builder(
      itemCount: meals.length,
      itemBuilder: (ctx, index) => MealItem(
        meal: meals[index],
        onTapHandler: () =>
            onTapMealHandler(context: context, meal: meals[index]),
      ),
    );

    if (meals.isEmpty) {
      content = Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              'Uh Oh... nothing here',
              style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                    color: Theme.of(context).colorScheme.onBackground,
                  ),
            ),
            const SizedBox(
              height: 15,
            ),
            Text(
              'Try selecting different category !',
              style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                    color: Theme.of(context).colorScheme.onBackground,
                  ),
            ),
          ],
        ),
      );
    }

    return content;
  }
}
