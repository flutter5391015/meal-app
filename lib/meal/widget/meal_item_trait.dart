import 'package:flutter/material.dart';

class MaterialTrait extends StatelessWidget {
  final IconData iconData;
  final String label;
  const MaterialTrait({super.key, required this.iconData, required this.label});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(
          iconData,
          size: 17,
          color: Colors.white,
        ),
        const SizedBox(
          width: 6,
        ),
        Text(
          label,
          style: const TextStyle(
            color: Colors.white,
          ),
        ),
      ],
    );
  }
}
