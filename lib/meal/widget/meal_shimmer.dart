import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class MealShimmer extends StatefulWidget {
  final Widget child;
  const MealShimmer({super.key, required this.child});

  @override
  State<MealShimmer> createState() => _MealShimmerState();
}

class _MealShimmerState extends State<MealShimmer> {
  bool isLoading = true;

  void initState() {
    super.initState();

    // simulate loading data
    Future.delayed(const Duration(seconds: 2), () {
      setState(() {
        isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return isLoading
        ? Shimmer(
            gradient: const LinearGradient(
              colors: [
                Color(0xFFEBEBF4),
                Color(0xFFF4F4F4),
                Color(0xFFEBEBF4),
              ],
              stops: [
                0.1,
                0.3,
                0.4,
              ],
              begin: Alignment(-1.0, -0.3),
              end: Alignment(1.0, 0.3),
              tileMode: TileMode.clamp,
            ),
            period: const Duration(seconds: 500),
            child: widget.child)
        : widget.child;
  }
}
