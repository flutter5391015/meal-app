import 'package:flutter/material.dart';
import 'package:meals_app/category/model/category.dart';

class CategoryGrid extends StatelessWidget {
  final void Function() onTapHandler;
  final Category category;
  final bool isDetailExists;
  const CategoryGrid(
      {super.key,
      required this.category,
      required this.onTapHandler,
      required this.isDetailExists});

  @override
  Widget build(BuildContext context) {
    List<Color> bodyColors = [
      category.color.withOpacity(0.55),
      category.color.withOpacity(0.9)
    ];

    if (isDetailExists) {
      bodyColors = [
        Colors.grey.shade400,
        Colors.grey,
      ];
    }
    return InkWell(
      onTap: onTapHandler,
      splashColor: Theme.of(context).primaryColor,
      borderRadius: BorderRadius.circular(
          16), // untuk buat border lebih bulet ketika di tap
      child: Container(
        padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          gradient: LinearGradient(
            colors: bodyColors,
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
        ),
        child: Text(
          category.title,
          style: Theme.of(context).textTheme.titleLarge!.copyWith(
              color: Theme.of(context).colorScheme.onBackground, fontSize: 16),
        ),
      ),
    );
  }
}
