import 'package:flutter/material.dart';
import 'package:meals_app/category/data/dummy_category.dart';
import 'package:meals_app/category/widget/category_grid.dart';
import 'package:meals_app/meal/meal_screen.dart';
import 'package:meals_app/meal/model/meal.dart';

class CategoryScreen extends StatefulWidget {
  final List<Meal> filteredMeals;
  const CategoryScreen({super.key, required this.filteredMeals});

  @override
  State<CategoryScreen> createState() => _CategoryScreenState();
}

//SingleTickerProviderStateMixin ini buat single animation, klo banyak animasi pake TickerProviderStateMixin
class _CategoryScreenState extends State<CategoryScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController animationController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 300),
      lowerBound: 0,
      upperBound: 1,
    );
    animationController.forward();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    animationController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    /*
    cara manggilnya bisa seperti ini
    final List<Widget> listOfCategory = [];
    for (var category in availableCategories) {
      listOfCategory.add(
        CategoryGrid(category: category),
      );
    }*/

    void onTapCategoryHandler(
        {required BuildContext context,
        required String title,
        required List<Meal> meals}) {
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (ctx) => MealScreen(
            title: title,
            meals: meals,
          ),
        ),
      );
    }

    return AnimatedBuilder(
      animation: animationController,
      child: GridView(
        padding: const EdgeInsets.all(24),
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 3 / 2,
            crossAxisSpacing: 20,
            mainAxisSpacing: 20),
        children: availableCategories.map(
          (e) {
            final isMealExists = widget.filteredMeals.where(
              (element) => element.categories.contains(e.id),
            );

            return CategoryGrid(
              isDetailExists: isMealExists.isEmpty,
              category: e,
              onTapHandler: () {
                if (isMealExists.isNotEmpty) {
                  onTapCategoryHandler(
                      context: context,
                      meals: widget.filteredMeals
                          .where(
                            (element) => element.categories.contains(e.id),
                          )
                          .toList(),
                      title: e.title);
                } else {
                  ScaffoldMessenger.of(context).clearSnackBars();
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      backgroundColor: Colors.green,
                      content: Text(
                        'No data available',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  );
                }
              },
            );
          },
        ).toList(),
      ),
      builder: (ctx, child) => SlideTransition(
        position: Tween(
          begin: const Offset(0, 0.3),
          end: const Offset(0, 0),
        ).animate(
          CurvedAnimation(
            parent: animationController,
            curve: Curves.easeIn,
          ),
        ),
        child: child,
      ),
    );
  }
}
