import 'package:flutter/material.dart';
import 'package:meals_app/category/category_screen.dart';
import 'package:meals_app/filter/filter_screen.dart';
import 'package:meals_app/main_drawer.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:meals_app/meal/meal_screen.dart';
import 'package:meals_app/filter/provider/filter_meal_provider.dart';
import 'package:meals_app/meal/provider/fav_meal_provider.dart';

final kInitialFilter = {
  Filter.glutenFree: false,
  Filter.lactoseFree: false,
  Filter.vegan: false,
  Filter.vegetarian: false,
};

class TabScreen extends ConsumerStatefulWidget {
  const TabScreen({super.key});

  @override
  ConsumerState<TabScreen> createState() => _TabScreenState();
}

class _TabScreenState extends ConsumerState<TabScreen> {
  int selectedIndex = 0;

  void onTapHandler(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  void setScreen(String identifier) async {
    if (identifier == 'filters') {
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (ctx) => const FilterScreen(),
        ),
      );
    } else {
      Navigator.of(context).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    // ini untuk melihat apakah ada perubahan atau tidak
    final favMeal = ref.watch(favoriteMealProvider);
    final availableMeals = ref.watch(filteredMealProvider);

    String title = 'Categories';
    Widget activeScreen = CategoryScreen(
      filteredMeals: availableMeals,
    );

    if (selectedIndex != 0) {
      activeScreen = MealScreen(
        meals: favMeal,
      );
      title = 'Favorites';
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: activeScreen,
      drawer: MainDrawer(
        onSelectScreen: setScreen,
      ),
      drawerEnableOpenDragGesture: true,
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: selectedIndex,
        onTap: onTapHandler,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.set_meal),
            label: 'Categories',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'Favorites',
          ),
        ],
      ),
    );
  }
}
