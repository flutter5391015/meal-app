import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:meals_app/filter/provider/filter_meal_provider.dart';

class FilterItem extends ConsumerWidget {
  final bool value;
  final String title;
  final String subtitle;
  const FilterItem(
      {super.key,
      required this.value,
      required this.title,
      required this.subtitle});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return SwitchListTile(
      value: value,
      onChanged: (newValue) {
        ref
            .read(filterProvider.notifier)
            .setFilter(Filter.glutenFree, newValue);
      },
      title: Text(
        title,
        style: Theme.of(context).textTheme.titleLarge!.copyWith(
              color: Theme.of(context).colorScheme.onBackground,
            ),
      ),
      subtitle: Text(
        subtitle,
        style: Theme.of(context).textTheme.labelMedium!.copyWith(
              color: Theme.of(context).colorScheme.onBackground,
            ),
      ),
      activeColor: Theme.of(context).colorScheme.tertiary,
      contentPadding: const EdgeInsets.only(left: 34, right: 20),
    );
  }
}
