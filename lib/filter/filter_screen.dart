import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:meals_app/filter/provider/filter_meal_provider.dart';
import 'package:meals_app/filter/widget/filter_item.dart';

class FilterScreen extends ConsumerWidget {
  const FilterScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final activeFilters = ref.watch(filterProvider);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Filter'),
      ),
      body: Column(
        children: [
          FilterItem(
              value: activeFilters[Filter.glutenFree]!,
              title: 'Gluten-free',
              subtitle: 'Only include gluten-free meals'),
          FilterItem(
              value: activeFilters[Filter.lactoseFree]!,
              title: 'Lactose-free',
              subtitle: 'Only include lactose-free meals'),
          FilterItem(
              value: activeFilters[Filter.vegetarian]!,
              title: 'Vegetarian',
              subtitle: 'Only include vegetarian meals'),
          FilterItem(
              value: activeFilters[Filter.vegan]!,
              title: 'Vegan',
              subtitle: 'Only include vegan meals'),
        ],
      ),
    );
  }
}
