import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:meals_app/meal/provider/meal_provider.dart';

enum Filter { glutenFree, lactoseFree, vegetarian, vegan }

class FilterNotifier extends StateNotifier<Map<Filter, bool>> {
  FilterNotifier()
      : super(
          {
            Filter.glutenFree: false,
            Filter.lactoseFree: false,
            Filter.vegetarian: false,
            Filter.vegan: false,
          },
        );

  void setFilters(Map<Filter, bool> chosenFilter) {
    state = chosenFilter;
  }

  void setFilter(Filter filter, bool isActive) {
    state = {
      ...state,
      filter: isActive,
    };
  }
}

final filterProvider = StateNotifierProvider<FilterNotifier, Map<Filter, bool>>(
  (ref) => FilterNotifier(),
);

final filteredMealProvider = Provider((ref) {
  final meals = ref.watch(mealProvider);
  final activeFilter = ref.watch(filterProvider);

  return meals.where((el) {
    if (activeFilter[Filter.glutenFree]! && !el.isGlutenFree) return false;

    if (activeFilter[Filter.lactoseFree]! && !el.isLactoseFree) {
      return false;
    }

    if (activeFilter[Filter.vegetarian]! && !el.isVegetarian) {
      return false;
    }
    if (activeFilter[Filter.vegan]! && !el.isVegan) {
      return false;
    }

    return true;
  }).toList();
});
